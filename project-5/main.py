"""
Breaking out of Loop
"""

some_number = 10

while(some_number != 0):
    if(some_number <= 10):
       some_number = some_number - 3
       print(some_number)

    if(some_number<= -10):
        break


"""
Class
"""
# classes are advised to follow camelcase


class Grades:
    min = 0
    max = 100

    def __init__(self, scoreMath, scorePhy, scoreDesign):
        self.scoreDesign = scoreDesign
        self.scoreMath = scoreMath
        self.scorePhy = scorePhy

    def percentage(self):
        sum = self.scoreDesign + self.scoreMath + self.scorePhy
        percent = sum/3

        print(percent)


az21 = Grades(90, 70, 80)
az21.percentage()
