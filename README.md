# Learning Python

This is a personal project to learn Python 3.

NOTE: `project-1` and `project-2` are optional. I wasn't satified with them because the notes weren't readable. `project-3` is the intended start of the project which follows the order of topics defined in the LaTeX pdf.

## [LaTeX PDF][1]

## Topics Covered
1. Comments
2. Print
3. Variables
4. Index Element
5. String Manipulation
6. Concatenation
7. Print Format
8. List/Array Manipulation and Matrix






[1]:https://github.com/Az-21/learning-python/blob/master/reference/python-ref.pdf