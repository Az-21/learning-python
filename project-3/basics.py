'''============================'''
'''Print                       '''
'''============================'''

print("Hello World\n");


'''============================'''
'''Variables and Basic Math    '''
'''============================'''

x = 2;
y = 3;
z = x + y;
print(z);
z = x - y;
print(z);
z = x * y;
print(z);
z = x / y;
print(z);

print(5**3);
print("\n")


'''============================'''
'''Array and Index             '''
'''============================'''
numArray = [1, 4, 9, 16, 25];
numString = "abc 123";

print(numArray[0]);
print(numArray[1]);
print(numArray[2]);
print(numArray[3]);
print(numArray[4]);
print("\n");

print(numString[0]);
print(numString[1]);
print(numString[2]);
print(numString[3]); #Spaces have their own index
print(numString[4]);
print(numString[5]);
print(numString[6]);
print("\n");

numStringModified = numString[4: ];
print(numStringModified);
numString = numString[ :3];
print(numString);

'''============================'''
'''Concatenation               '''
'''============================'''

numString = numString + " 123";
print(numString);

hybridString = numString + " "  + numString;
print(hybridString);

'''============================'''
'''String Manipulation         '''
'''============================'''


myString = "HeLlO eVeRyOnE";

myString = myString.lower();    #to Lowercase
print(myString);

myString = myString.upper();    #to Uppercase
print(myString);

myString = myString.split(" "); #Split at space
print(myString);

'''============================'''
'''Print Format               '''
'''============================'''

num1 = 90;
num2 = 100;

print("Score = %s out of %s" %(num1, num2));
print("Score = {min} / {max}".format(min = num1, max = num2));


array1 = [1, 2, 3];
array2 = ["Hello", 1];
array3 = array1 + array2;
print(array3);