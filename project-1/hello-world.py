print("Hello world")

#-------------------------------------------------#
#-------------------------------------------------#

# Data Types. Use "type" to find the data Types
# This works in console. I've written the output as comment

'''
type(2);                #int
type(2.0);              #float
type(-2);               #int
type('a');              #string (single letter string = char); NO char in py
'''


#-------------------------------------------------#
#Variables and Data Types
#-------------------------------------------------#

# Variables
'''
number_1 = 100;
number_2 = 200;
my_string = "Hello";
'''

# Using Variables (basic)
'''
type(number_1);         #int
print(number_1);
print(my_string);
'''

# Multivariable assignment

#Case 1
'''
a = b = c = 1.55;

print(a);
print(b);
print(c);
'''

#Case 2
'''
var_1, var_2, var_3 = 1, 1.44, "alpha";

print(var_1);
print(var_2);
print(var_3);
'''

# Note on Initilization
#Python does not require initilization, but it is recommended to comment the intended initilization

# Reserved names list [import keyword -> keywork.kwlist]
#['False', 'None', 'True', 'and', 'as', 'assert', 'break', 'class', 'continue', 'def', 'del', 'elif', 'else', 'except', 'finally', 'for', 'from', 'global', 'if', 'import', 'in', 'is', 'lambda', 'nonlocal', 'not', 'or', 'pass', 'raise', 'return', 'try', 'while', 'with', 'yield']


#-------------------------------------------------#
#Clear python terminal
#-------------------------------------------------#

# Clear screen (for terminal)
'''
     >>> import os                                         #Step 1
     >>> clear = lambda: os.system('cls')                  #Step 2
     >>> clear                                             #Step 3
'''

#-------------------------------------------------#
#Comments
#-------------------------------------------------#

# Single line Comments

# Pound/Hashtag defiens a coment. Spans until a new line i.e. inline Comment
# Pound/Hashtag is included in a string and it will be displayed on print

# Multiline Comments

''' Start

End '''

#-------------------------------------------------#
#Basic Math
#-------------------------------------------------#


# Basic Math

'''
print(2+5);                 #addition
print(2-5);                 #subtraction
print(2*5);                 #multiplication
print(2/5);                 #division (python auto changes the data type)

print(2**5);                #raise to the power (** and ^ are different in python)
print(2//5);                #rounded down (int if int are used, float if float input is used)
print(5%2);                 #remainder
'''

# Operator hirearchy
# Same as IRL math: (**)  (*)  (/)  (%)  (+)  (-)

#-------------------------------------------------#
#Strings
#-------------------------------------------------#

# Strings

string_1 = 'Single quotes work';
string_2 = "As do double quotes";

# String Index
'''
print(string_1[0]);                 #S
print(string_1[1]);                 #i
print(string_1[6]);                 #whitespace
'''

#   str = "abcdefg"
#   ind                     0 1 2 3 4 5 6
#   str                     a b c d e f g
#   ind (-ve)               7 6 5 4 3 2 1

'''
print(string_1[-1]);                #k
print(string_1[-2]);                #r
'''

# Basic string functions
'''
print(len(string_1));               #Print the length of string_1
print(string_1 + " " + string_2);   #add strings
print(3*string_1);                  #Multiply stings (basically add)
'''

# String Format [Adding yet-to-know values to a string]

'''
print("{name}, Thank You. Your total is {0} which is inclusive of {1} VAT".format(total_money, total_tax, name = customer_name));
#           ("string {0} {1} {2}".format(what to put in 0,1,2,and so on))
#           {0} {1} first. {variable} later and in order of appearance
'''

# Special Characters [apost]

'''
#Incorrect: 'I am a string in "C++".'
            "I am a string in "C++"."

#Correct:   'I\'m a string in \"C++\"'
            "I\'m a string in \"C++\""
'''


#-------------------------------------------------#
#Bool Operators and Expressions
#-------------------------------------------------#


#List of bool operators
'''
           <           Less than
           >           Greater than
           <=          Less than OR equal to
           >=          Greater than OR equal to
           !=          NOT equal to
           ==          Equal to
'''


# Return values of bool operators
'''
print(10<11);               #True
print(11<10);               #False
'''


#List of logical operators (in order of priority)
'''
            not
            and
            or
'''


#Logical operator in use
'''
a = True;
b = False;

print(a or b);              #True or False = True
print(a or a);              #True or True = True
print(b or b);              #False or False = False
'''


# IF Statement
'''
if (<condition> and/or <condition>):
        statement to execute if the above statement is True
  ^^ <--------- This indentation is important. It is also need for nested if statements
'''


# If-else code
'''
a = 66;
if (a>67):
    print("a is greater than or equal to 68");
else:
    print("a is less than or equal to 67");
'''


# If-elseif-elseif
'''
if <bool exp>:
    do something;
elseif <bool exp>:
    do something else;
else <bool exp>:
    do something if every other bool exp is False
'''


#-------------------------------------------------#
#For Loops
#-------------------------------------------------#

#Syntax
'''
for <variable> in range(start, stop, step):                [stop is not included]
    do something

    AKA


for <variable> in range(initial, final, d of AP):           [final is not included]
    do something
'''

#For loop in use
'''
for i in range(0,10,2):
    print(i);


string_3 = "Hello there";
for i in range(0,len(string_3)):
    print(string_3[i]);
'''
