# Defining Functions
# When defining functions, leave 2 blank lines before and after the declaration of function.


def my_function():
    print("The function was called.")


def my_function_plus_arguments(num1, num2):
    num3 = num1 + num2
    return num3


def sentence(name, age):
    print("My name is " + name + ", and I'm " + str(age) + " years old.")


def sentence_with_defaults(name = "Default", age = -1):
    print("My name is " + name + ", and I'm " + str(age) + " years old.")


# Sentence with defaults // Overloading
sentence_with_defaults()
sentence_with_defaults("Az21")
sentence_with_defaults(age = 21)


'''
'''
'''
'''


"""
If-elseif
"""

number1 = 100
number2 = 200
string1 = "Hi"
string2 = "Hello"

if(number1 == number2):
    print("number1 = number2")
elif(number1 > number2):
    print("number1 > number2")
else:
    print("number1 < number2")


'''
'''
'''
'''

"""
While Loop
"""

index = 0
my_array = [2,4,8,16,32]
while(index < len(my_array)):
    print(my_array[index])
    index = index + 1

